import shutil
import tempfile
import atexit
import asyncio
import aiohttp
from pyppeteer import launch
from collections import Counter
from fuzzywuzzy import fuzz
from datetime import datetime, timedelta

DAY = 10
TEMP_DIR = tempfile.TemporaryDirectory()

print('Temp dir', TEMP_DIR)

class Client():
    def __init__(self):
        self.word_list = []

    async def run(self, days=0):
        self.client = aiohttp.ClientSession()

        self.correct_letters = ['','','','','']
        self.incorrect_letters = [[], [], [], [], []]
        self.misses = [] # All characters that are part of the final word but are not in the right place.

        self.browser = await launch(
            headless=False,
            autoClose=False,
            userDataDir=TEMP_DIR.name,
        )
        self.page = await self.browser.newPage()

        async def setup():
            timestamp = str(int((datetime.utcnow() - timedelta(hours=24 * days)).timestamp())) + '000'

            print('We\'re running this Lewdle for day:', (datetime.utcnow() - timedelta(hours=24 * days)).isoformat())

            await self.page.evaluateOnNewDocument("""
            () => {
                var _Date = Date,
                    _getTimezoneOffset = Date.prototype.getTimezoneOffset,
                    now = null
                function MockDate(y, m, d, h, M, s, ms) {
                    var date
                    switch (arguments.length) {
                        case 0:
                            if (now !== null) {
                                date = new _Date(now)
                            } else {
                                date = new _Date()
                            }
                            break
                        case 1:
                            date = new _Date(y)
                            break
                        default:
                            d = typeof d === 'undefined' ? 1 : d
                            h = h || 0
                            M = M || 0
                            s = s || 0
                            ms = ms || 0
                            date = new _Date(y, m, d, h, M, s, ms)
                            break
                    }

                    return date
                }
                MockDate.UTC = _Date.UTC
                MockDate.now = function() {
                    return new MockDate().valueOf()
                }
                MockDate.parse = function(dateString) {
                    return _Date.parse(dateString)
                }
                MockDate.toString = function() {
                    return _Date.toString()
                }
                MockDate.prototype = _Date.prototype

                function set(date, timezoneOffset) {
                    var dateObj = new Date(date)
                    if (isNaN(dateObj.getTime())) {
                        throw new TypeError(
                            'mockdate: The time set is an invalid date: ' + date
                        )
                    }
                    if (typeof timezoneOffset === 'number') {
                        MockDate.prototype.getTimezoneOffset = function() {
                            return timezoneOffset
                        }
                    }
                    Date = MockDate
                    now = dateObj.valueOf()
                }

                // mock date
                set([t])
            }

            """.replace('[t]', timestamp))

            await self.page.goto('https://www.lewdlegame.com/')

            await self.page.addStyleTag(url='https://unpkg.com/notie/dist/notie.min.css')
            await self.page.addStyleTag(url='https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;900&display=swap')
            await self.page.addScriptTag(url='https://unpkg.com/notie')
            await self.page.addStyleTag(content="""
                * {
                    font-family: 'Inter'!important;
                }
                .notie-textbox-inner {
                    font-size:0.6em;
                    text-align:left;
                }
            """)

            # Close first time messages because lord they wont leave me alone.
            await self.page.waitFor('.helpRow02')
            await (await self.page.querySelector('.helpRow02')).click()
            elm = await self.page.waitForSelector('.updateClose')
            await asyncio.sleep(1)
            await elm.click()

        coros = [self.get_word_data(), setup()]

        await asyncio.gather(*coros)

        # continue with our task.

        self.current_guess = 0

        for _ in range(0, 5):
            print('=' * 12, f'ROUND {_}', '=' * 12)

            keys = self.most_common(_)

            current_best = [0, '']

            for word in self.words:
                ratio = fuzz.ratio(''.join(keys), word)
                if ratio > current_best[0]:
                    current_best = [ ratio, word ]

            print('Current best guess:', current_best, ' Using characters: ', keys)

            for key in current_best[1]:
                await self.press_key(key)
            
            await self.press_enter()

            await asyncio.sleep(3) # ususally takes about 3 seconds to do the animations

            board_row = (await self.page.querySelectorAll('.boardRow'))[self.current_guess]

            # read app state
            index = 0
            for element in await board_row.querySelectorAllEval('.mainTile', '(nodes) => nodes.map(n => n.style.backgroundColor)'):
                if element == 'var(--correct)':
                    self.correct_letters[index] = current_best[1][index]
                elif element == 'var(--miss)':
                    # a miss is where the letter exists in the final word but we got the wrong placement.
                    # we should add this to the list of missed letters but also add it as an incorrect one.
                    # since we know its not meant for that place.
                    self.misses.append(current_best[1][index])
                    self.incorrect_letters[index].append(current_best[1][index])
                else:
                    self.incorrect_letters[index].append(current_best[1][index])

                index += 1

            if len([x for x in self.correct_letters if x]) == len(self.correct_letters):
                # we're done!!
                await self.notification('Finished!!', type='success')
                return

            self.filter_words()

            await self.notification('We\'re currently guessing these words:<br/>' + ', '.join(self.words) + f'<br/>Progress: {self.progress}%')

            self.current_guess += 1 # increment when we finish processing

    def filter_words(self):
        """
        Using self.correct_letters, remove words from our word list that dont match the spec.
        """

        new_words = []

        print('Guess incorrect, filtering word list...')

        print('Correct letters: ', self.correct_letters, '\nIncorrect letters: ', self.incorrect_letters, '\nMissing characters: ', self.misses)

        words = self.words

        filtered_new_words = []

        for word in words:
            if self.misses:
                for letter in self.misses:
                    if letter in word:
                        filtered_new_words.append(word)
                        break
            else:
                filtered_new_words.append(word)

        for word in filtered_new_words:
            matched = 0
            for i, letter in enumerate(self.correct_letters):
                if word[i] == letter:
                    matched += 1
            
            # if the total letters in the word is equal to the current guesses
            if matched == len([x for x in self.correct_letters if x]):
                new_words.append(word)

        filtered_new_words = []

        for word in new_words:
            # now remove all words that have a incorrect letter
            add_word = True
            for r in range(0, 4):
                for i, letter_arr in enumerate(self.incorrect_letters[r]):
                    for letter in letter_arr:
                        if word[r] == letter:
                            add_word = False
            if add_word:
                filtered_new_words.append(word)

        self.words = [x for x in sorted([x for x in set(filtered_new_words)])]

        def get_change(current, previous):
            if current == previous:
                return 100.0
            try:
                return (abs(current - previous) / previous) * 100.0
            except ZeroDivisionError:
                return 0

        self.progress = get_change(len(self.words), self.last_word_length)
        
        print('Current guess list:', self.words)

    async def get_word_data(self):
        # first work out the words we have to pick from.
        url = 'https://bwnew2n3u5cina42gveozinmkq.appsync-api.us-west-1.amazonaws.com/graphql'
        api_key = 'da2-awhynspbnzg3dcnxuis5vqyu4e'
        body = {"query":"query ListDicts($filter: ModelDictsFilterInput, $limit: Int, $nextToken: String) {\n  listDicts(filter: $filter, limit: $limit, nextToken: $nextToken) {\n    items {\n      id\n      Lewdles\n      LewdWords\n      createdAt\n      updatedAt\n      _version\n      _deleted\n      _lastChangedAt\n    }\n    nextToken\n    startedAt\n  }\n}\n","variables":{}}
        
        headers = {
            'x-api-key': api_key
        }

        async with self.client.post(url, json=body, headers=headers) as resp:
            data = await resp.json()

        words = [x for x in data['data']['listDicts']['items'][0]['Lewdles'][0].split(',') if x]

        self.words = words
        self.last_word_length = len(words)

    def most_common(self, index):
        most_common = []

        for i in range(0, 5):
            most_common.append(Counter([x[i] for x in self.words]).most_common(1)[0][0])

        return most_common

    async def notification(self, text, type='info'):
        await self.page.evaluate("""() => {
            notie.alert({ type: '[type]', text: "[text]", stay: true })
        }""".replace('[type]', type).replace('[text]', text))

    async def press_enter(self):
        await (await self.page.querySelectorAll('.doubleKey'))[0].click()

    async def press_key(self, key_to_press):
        rows = [
            'QWERTYUIOP',
            'ASDFGHJKL',
            'ZXCVBNM'
        ]

        key = key_to_press.upper()

        key_index = None
        
        rowID = 0
        for row in rows:
            if key in row:
                rindex = 1
                for letter in row:
                    if key == letter:
                        key_index = rindex
                    rindex += 1

            # dont loop if we have seen this letter on this row.
            if key_index == None:
                rowID += 1
            
        element = (await self.page.querySelectorAll('.keyboardRow'))[rowID]
        key_element = (await element.querySelectorAll('.mainKey' if rowID != 1 else '.middleKey'))[key_index - 1]
        await key_element.click()
            
    async def close(self):
        #await self.browser.close()
        pass

client = Client()

# def exit_handler():
#     shutil.rmtree(TEMP_DIR.name)

# atexit.register(exit_handler)

inp = input('What day do you wish to run on? => ')

DAYS = int(0 if not inp else inp)

asyncio.get_event_loop().run_until_complete(client.run(days=DAYS))
asyncio.get_event_loop().run_until_complete(client.close())
