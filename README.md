# Lewdle Solver

A Lewdle solver using funny things like math and science 🤖 ⚡
Solves most Lewdles in 3-4 steps.

Got questions? Find me on Discord: HornyDrone#0967, or on Twitter: [@HorniBlue](https://twitter.com/HorniBlue)

## Running
OK so, you need a few things before running this script for the first time. You first need Python 3.6 or higher. The script makes use of packages that require that version or higher, sorry!

The AI only needs two external packages so install them like:
```py
pip install pyppeteer
pip install fuzzywuzzy
```

![](https://i.imgur.com/TzPM2Aj.png)

Once everything is installed, just run it like `python lewdle.py`.
You should see a prompt asking what day you want to run the bot on. Press enter to do todays or type a number (positive for previous Lewdles, negitive for future Lewdles).

```bash
python lewdle.py

What day do you wish to run on? => -1
```

For tomorrows Lewdle, for example.

## But Drone! How does it work??
Good question! I dont know, it just does.

For real though, it gets the most common letters for each column and fuzzy searches for a word that matches that word. Thats why the AI always starts with `BONER`, thats the word with the most common letters. Then it just repeats, removing words if we know characters cant be in that place, removing words that dont have the found characters, etc until theres only one word left.

At this rate, it solves most Lewdles in 3-4 steps. Fun fact: `BONER` removes 50% of the pool of words in a single guess.

## License
Licenced under MIT. Please dont claim this project as your own, otherwise we cool 💙

## Project status
Uh, unless Lewdle changes in the future, this project wont be seeing many updates i dont think.
